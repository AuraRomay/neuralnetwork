#pragma once
#include <vector>
#include <iostream>
#include "Neurona.h"

class Neurona;

struct Layer{
	std::vector<Neurona> layerNeuronas;
};

class RedNeuronal
{
public:
	RedNeuronal(std::vector<int> esqueleto);
	~RedNeuronal();

	void forwardProp(std::vector<float> inputs);
	void backProp();
	void print() {
		std::cout << misLayers[2].layerNeuronas[0].getOutput();
	}

private:
	std::vector<Layer> misLayers;
	float error;
};



#include "RedNeuronal.h"

//output 25
//12 capa escondida
//12 output

RedNeuronal::RedNeuronal(std::vector<int> esqueleto)
{
	int tempLayers = esqueleto.size();

	for (size_t i = 0; i < tempLayers; i++)
	{
		Layer tempNeurona;
		misLayers.push_back(tempNeurona);
		
		int numeroVecinos;
		if (i == esqueleto.size() - 1) {
			numeroVecinos = 0;
		}
		else {
			numeroVecinos = esqueleto[i + 1];
		}

		for (size_t j = 0; j < esqueleto[i]; j++)
		{
			Neurona tempN(numeroVecinos, j);
			misLayers.back().layerNeuronas.push_back(tempN);
		}
	}
}


RedNeuronal::~RedNeuronal()
{
}

void RedNeuronal::forwardProp(std::vector<float> inputs)
{
	for (size_t i = 0; i < inputs.size(); i++)
	{
		misLayers[0].layerNeuronas[i].setOutput(inputs[i]);
	}

	for (size_t i = 1; i < misLayers.size(); i++)
	{
		std::vector<Neurona> prevLayer = misLayers[i - 1].layerNeuronas;
		for (size_t j = 0; j < misLayers[i].layerNeuronas.size(); j++)
		{
			misLayers[i].layerNeuronas[j].fowardProp(prevLayer);
		}
	}
}

void RedNeuronal::backProp()
{
	float spectedValue = 1.f;
	error = 0.0f;

	float calculoCorrecto = spectedValue - misLayers[2].layerNeuronas[0].getOutput();
	error = calculoCorrecto * calculoCorrecto;

	error = sqrt(error);

	misLayers[2].layerNeuronas[0].calcularGradients(spectedValue);

	for (size_t i = misLayers.size() - 2; i > 0; i--)
	{
		for (size_t j = 0; j < misLayers[i].layerNeuronas.size(); j++)
		{
			misLayers[i].layerNeuronas[j].calcularHiddens(misLayers[i+1].layerNeuronas);
		}
	}

	for (size_t i = misLayers.size() - 1; i > 0; i--)
	{
		for (size_t j = 0; j < misLayers[i].layerNeuronas.size(); j++)
		{
			misLayers[i].layerNeuronas[j].updateWeights(misLayers[i - 1].layerNeuronas);

		}
	}
}

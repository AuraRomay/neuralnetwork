#pragma once
#include <vector>
#include <cstdlib>
#include <math.h>

struct Conexion {
	float weight;
	float tempWeight;
};

class Neurona
{
public:
	Neurona(int neuronasVecinas, int id);
	~Neurona();
	
	int neuronaId;
	float gradient;
	float eta = 0.1f;
	float alfa = 0.5f;

	void setOutput(float w) {
		output = w;
	}
	float getOutput() {
		return output;
	}

	void fowardProp(std::vector<Neurona> layerAnterior);
	void calcularGradients(float grad);
	void calcularHiddens(std::vector<Neurona> layerHidden);
	void updateWeights(std::vector<Neurona> &update);

private:
	float output;
	std::vector<Conexion> pesoSalida;

	static float randomWeight() {
		float porfavor = 0.0f;
		porfavor = rand() % 30 + 1;
		return porfavor/ 100;
		//return rand()/ float (RAND_MAX);
	}
	float funcSigmoide(float x);
	float funcSigmoideD(float x);
};


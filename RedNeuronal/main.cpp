#include <iostream>
#include <vector>
#include "RedNeuronal.h"
#include <time.h>

float tempR[]
{
	0.001,0.8,0.8,0.8, 0.001,
	0.001,0.8, 0.001,0.8, 0.001,
	0.001,0.8,0.8,0.8, 0.001,
	0.001,0.8, 0.001,0.8, 0.001,
	0.001,0.8, 0.001, 0.001,0.8,
};


int main() {
	srand(time(NULL));

	std::vector<int> esqueleto;

	std::vector<float> laLetra;

	for (size_t i = 0; i < 25; i++)
	{
		laLetra.push_back(tempR[i]);
	}
	//CREACION DE CAPAS
	esqueleto.push_back(25);
	esqueleto.push_back(12);
	esqueleto.push_back(1);

	RedNeuronal redN(esqueleto);

	//redN.forwardProp(laLetra);
	

	while (true) {
		redN.forwardProp(laLetra);
		redN.backProp();
		redN.print();
		system("pause");
	}

	return 0;
}
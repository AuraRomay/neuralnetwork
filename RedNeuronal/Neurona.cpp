#include "Neurona.h"

Neurona::Neurona(int neuronasVecinas, int id)
{
	for (size_t i = 0; i < neuronasVecinas; i++)
	{
		pesoSalida.push_back(Conexion());
		pesoSalida.back().weight = randomWeight();
	}

	neuronaId = id;
}

Neurona::~Neurona()
{
}

void Neurona::fowardProp(std::vector<Neurona> layerAnterior)
{
	float sum = 0.0f;

	for (size_t i = 0; i < layerAnterior.size(); i++)
	{
		sum += layerAnterior[i].getOutput() * 
			   layerAnterior[i].pesoSalida[neuronaId].weight;
	}

	output = funcSigmoide(sum);
}

void Neurona::calcularGradients(float grad)
{
	float temp = grad - output;
	gradient = temp * funcSigmoideD(output);
}

void Neurona::calcularHiddens(std::vector<Neurona> layerHidden)
{
	float sum = 0.0f;
	for (size_t i = 0; i < layerHidden.size(); i++)
	{
		sum += pesoSalida[i].weight * layerHidden[i].gradient;
	}

	gradient = sum * funcSigmoideD(output);
}

void Neurona::updateWeights(std::vector<Neurona> &update)
{
	for (size_t i = 0; i < update.size(); i++)
	{
		float oldTempW = update[i].pesoSalida[neuronaId].tempWeight;

		float newTempW = eta * update[i].getOutput() * gradient;
		newTempW += alfa * oldTempW;
		update[i].pesoSalida[neuronaId].tempWeight = newTempW;
		update[i].pesoSalida[neuronaId].weight += newTempW;
	}
}

float Neurona::funcSigmoide(float x)
{
	float porfavor = 0.0f;
	porfavor = 1 + exp(-1 * x);
	return 1 /porfavor;
}

float Neurona::funcSigmoideD(float x)
{
	float porfavor = 0.0f;
	porfavor = 1 + exp(-x + 1);
	porfavor = porfavor * porfavor;
	return exp(-x) / porfavor;
}
